// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

class silentLog {
    static debug() {}
    static info() {}
    static warn() {}
    static error() {}
    static group() {}
    static groupEnd() {}
}

const log = process.env.NODE_ENV == 'test' ? silentLog : console;
// const log = console;

export default log;
