// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import s3 from '../services/S3.js';
import log from './logger.js';

import os from 'os';
import path from 'path';
import fs from 'fs/promises';

import transliterate from '@sindresorhus/transliterate';
import encoding from '../../data/unicodeToTeletextMapping.json';
import { JSDOM } from 'jsdom';

const decodedCache = {};

export class Utils {

    // "base64url" encoding defined here https://tools.ietf.org/html/rfc4648
    // the packed data format is from https://github.com/rawles/edit.tf
    // returns array of rows
    static decodeBase64URLEncoded(originalInput) {
        if (originalInput in decodedCache) {
            return [...decodedCache[originalInput]];
        }

        // adjust the input from base64url to base64
        let input = originalInput.replace(/-/g, '+').replace(/_/g, '/');
        const pad = input.length % 4;
        if (pad) {
            if (pad === 1) throw new Error('Utils.decodeBase64URLEncoded E16: Input base64url string is the wrong length to determine padding');
            input += new Array(5-pad).join('=');
        }
        const bytes = Buffer.from(input, 'base64');
        let bits = [];
        bytes.forEach(b => {
            bits.push(b.toString(2).padStart(8, '0').split(''));
        });
        bits = bits.flat();

        decodedCache[originalInput] = getUnpackedData(bits);
        return [...decodedCache[originalInput]];
    }

    static mapChars(string, charset) {
        if (!(charset in encoding)) throw new Error(`E29 mapChars: bad charset: ${charset}`);

        // 0. replace particular 7-bit chars that will cause problems as they're remapped by teletext g0 sets
        let newString = string.replace(/\[/g, '(');
        newString = newString.replace(/\]/g, ')');

        // 1. replace unicode chars with their teletext charset equivalents
        newString = [...newString].map(c => c in encoding[charset] ? encoding[charset][c] : c).join('');

        // 2. replace other characters with nearest 7-bit equivalents
        newString = transliterate(newString, {
            customReplacements: [
                ['‘', "'"],
                ['’', "'"],
                ['—', '-'], // em dash
                ['–', '-'], // en dash
                ['‑', '-'], // hyphen
                ['\u00ad', ''], // soft hyphen
                ['“', '"'],
                ['”', '"'],
                ['€', 'EUR'],
                ['…', '...'],
                ['°', ' degrees '],
                ['•', '\x7f'], // maps to a square block in g0 sets
                ['😬', ':-s'], // grimace
                [' ', ''], // hair space \u200a
                [' ', ''], // thin space \u2009
                ['\u200b', ''], // zero width space
                ['\u00a0', ' '], // no break space
                ['\u2069', ''], // pop directional isolate
            ]
        });

        // 3. replace any left over 8-bit or more characters with a space
        // eslint-disable-next-line no-control-regex
        const result = newString.replace(/[^\x0a\x20-\x7F]/g, ' ');
        if (result != newString) {
            log.warn(`Replaced: ${newString}\n    with: ${result}`);
            [...newString].forEach(c => {
                const code = c.charCodeAt(0);
                if (code >= 127) {
                    log.warn(` char: ${c} dec code: ${code} hex code: \\u${Number(code).toString(16)}`);
                }
            });
        }
        return result;
    }

    static convertHTMLToParagraphs(html) {
        html = html.replace(/<br><br>/g, '</p><p>'); // FUDGE

        const dom = JSDOM.fragment(html);
        const paragraphs = [];
        for (const p of dom.children) {
            if (p.nodeName == 'P' && p.firstChild) { // non-empty paragraph
                if (p.textContent) {
                    let para = p.textContent;
                    if (p.innerHTML.match(/<br>/)) {
                        p.innerHTML = p.innerHTML.replace(/<br>/g, '\n');
                        para = p.textContent;
                    }
                    para = para.trimEnd();
                    if (para) paragraphs.push(para);
                }
            }
        }
        return paragraphs;
    }

    static justifyItems(length, items) {
        if (items.length < 2)
            return items.join('');

        let result = '';
        const numItems = items.length;
        for (let i = 0; i < numItems; i++) {
            result += items.shift();
            const totalLength = result.length + items.join('').length;
            const gapLength = Math.floor((length - totalLength) / items.length);
            result += "".padEnd(gapLength, ' ');
        }

        return result;
    }

    // returns a regex replacer function that replaces the matched string with a string padded to the same length
    static replaceAndPad(newStr) {
        return (match) => newStr.padEnd(match.length, ' ');
    }

    static async createEmptyDir(dir) {
        try {
            const outputDir = path.join(os.tmpdir(), dir);
            let files = [];
            try {
                files = await fs.readdir(outputDir);
            } catch (e) {
                await fs.mkdir(outputDir);
            }
            if (files.length) {
                const promises = [];
                files.forEach(f => {
                    promises.push(fs.unlink(path.join(outputDir, f)));
                });
                await Promise.all(promises);
            }
            return outputDir;
        } catch (e) {
            log.error(`E133 createEmptyDir failed for dir ${dir}`);
            log.error(e);
            throw new Error(`E133 createEmptyDir failed for dir ${dir}`);
        }
    }

    // expiryDate is optional
    static async copyFileToS3(filename, file, bucket, expiryDate) {
        const s3params = {
            Body: file,
            Bucket: bucket,
            Key: filename,
            ContentType: getContentType(filename)
        };
        if (expiryDate) {
            Object.assign(s3params, {
                CacheControl: 'public',
                Expires: expiryDate
            });
        }

        try {
            await s3.putObject(s3params).promise();
        } catch (e) {
            log.error('E150 copyFileToS3 exception:');
            log.error(e);
            throw new Error('E150 copyFileToS3 exception');
        }
    }

    static async s3ObjectExists(filename, bucket) {
        try {
            await s3.headObject({
                Bucket: bucket,
                Key: filename
            }).promise();
            return true;
        } catch (e) {
            if (e.code && e.code == 'NotFound') {
                return false;
            }
            log.error('s3ObjectExists exception:');
            log.error(e);
            throw new Error('E197 s3ObjectExists exception');
        }
    }
}

function getContentType(filename) {
    if (filename.match(/\.json$/)) {
        return 'application/json'
    }
    return 'application/octet-stream';
}

function getUnpackedData(bitArray) {
    // const firstBitIndex = (280 * row) + (7 * col);
    const page = [];

    for (let r = 0; r < 25; r++) {
        const rowChars = [];
        for (let c = 0; c < 40; c++) {
            let bitSignificance = 6;
            let charCode = 0;
            const firstBitIndex = (280 * r) + (7 * c);
            for (let bit = firstBitIndex; bit < firstBitIndex + 7; bit++) {
                charCode += bitArray[bit] * Math.pow(2, bitSignificance);
                bitSignificance--;
            }
            rowChars.push(String.fromCharCode(charCode));
        }
        page.push(rowChars.join(''));
    }
    return page;
}
