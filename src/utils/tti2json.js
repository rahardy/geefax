// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import log from './logger.js';
import { FileSaver } from '../services/FileSaver.js';

import fs from 'fs/promises';
import path from 'path';

// used for calculating the Expires header
const JSON_EXPIRY_TIME_MS = 10*60*1000; // 10 mins

class Service {
    constructor() {
        this.pages = {};
    }

    setSubpage(page, subpage, outputLines, encoding, fastext, webUrl, imageName) {
        if (!(page in this.pages)) {
            this.pages[page] = {
                subpages: []
            };
        }
        if (webUrl != null)
            this.pages[page].webUrl = webUrl;

        if (imageName != null)
            this.pages[page].image = imageName;

        this.pages[page].subpages[subpage] = {
            outputLines: outputLines,
            encoding: encoding
        };
        if (fastext != null)
            this.pages[page].subpages[subpage].fastext = fastext.object();
    }

    get hasPages() {
        return Object.keys(this.pages).length != 0;
    }

    clear() {
        this.pages = {};
    }
}

class PageStatus {
    constructor(code) {
        this._bits = Number(`0x${code}`).toString(2).padStart(16, 0).split('');

        // bits c12, c13, c14 from header row mapped to language in teletext spec
        this._languages = {
            "000": "g0_latin__english",
            "001": "g0_latin__german",
            "010": "g0_latin__swedish_finnish_hungarian",
            "011": "g0_latin__italian",
            "100": "g0_latin__french",
            "101": "g0_latin__portuguese_spanish",
            "110": "g0_latin__czech_slovak",
            "111": "g0_greek",
        };
    }

    encoding() {
        // bits 6, 7, 8 = c14, c13, c12
        const langBits = this._bits.slice(6, 9).reverse().join('');
        return this._languages[langBits];
    }

}

class Fastext {
    constructor(data) {
        const links = data.split(',');
        this.red = links[0].toUpperCase();
        this.green = links[1].toUpperCase();
        this.yellow = links[2].toUpperCase();
        this.blue = links[3].toUpperCase();
        this.link5 = links[4].toUpperCase();
        this.index = links[5].toUpperCase();

        this._obj = {};
        for (const link of ['red', 'green', 'yellow', 'blue', 'index']) {
            if (this[link] != 0) this._obj[link] = this[link];
        }
    }

    object() {
        return this._obj;
    }
}


const service = new Service();

function bufferToLines(buffer) {
    const lines = [];
    let line  = "";
    for (const byteCode of [...buffer]) {
        if (byteCode == 10) { // \n
            lines.push(line);
            line = "";
        } else {
            line += String.fromCharCode(byteCode);
        }
    }
    if (line.length != 0) lines.push(line);
    return lines;
}

// *.tti file format  https://zxnet.co.uk/teletext/documents/ttiformat.pdf
// extended with WL command for a web url
function getPagesFromTti(buffer) {
    const lines = bufferToLines(buffer);
    let pageNumber = 100;
    let subPage = 1;
    let outputLines = [];
    let encoding = null;
    let fastext = null;
    let webUrl = null;
    let imageName = null;
    for (const line of [...lines]) {
        const matches = line.match(/([A-Z]{2}),(.+)/);
        if (matches != null) {
            const command = matches[1];
            const data = matches[2];
            if (command == 'PN') {
                const m = data.match(/(\d[0-9A-Fa-f]{2})(\d\d)/);
                if (m != null) {
                    if (outputLines.length) {
                        service.setSubpage(pageNumber, subPage, outputLines.join("\n"), encoding, fastext, webUrl, imageName);
                    }
                    pageNumber = m[1].toUpperCase();
                    subPage = parseInt(m[2]);
                    outputLines = [];
                    fastext = null;
                }
            } else if (command == 'OL') {
                outputLines.push(matches[0]);
            } else if (command == 'PS') {
                const ps = new PageStatus(data);
                encoding = ps.encoding();
            } else if (command == 'FL') {
                fastext = new Fastext(data);
            } else if (command == 'WL') {   // extension
                webUrl = data;
            } else if (command == 'WI') {   // extension
                imageName = data;
            }
        }
    }
    if (outputLines.length)
        service.setSubpage(pageNumber, subPage, outputLines.join("\n"), encoding, fastext);
}

export async function tti2json(sourceDir, outputOptions) {
    log.debug('Creating JSON from TTIs');
    const allFiles = await fs.readdir(sourceDir);
    const saver = new FileSaver(outputOptions);

    const filesCreated = [];
    const promises = [];
    for (const magazine of [1,2,3,4,5,6,7,8]) {
        const filesRegEx = `^P${magazine}.*\\.tti`;
        const files = allFiles.filter(f => f.match(filesRegEx));
        for (const file of files) {
            const buffer = await fs.readFile(path.join(sourceDir, file));
            getPagesFromTti(buffer);
        }

        if (service.hasPages) {
            const filename = `${magazine}.json`;
            const expiryDate = new Date(Date.now() + JSON_EXPIRY_TIME_MS);
            promises.push(saver.save(filename, JSON.stringify(service), expiryDate));
            filesCreated.push(filename);
        }
        service.clear();
    }
    await Promise.all(promises);
    log.info(`tti2json ${saver.log}: ${filesCreated.join(', ')}`);
}
