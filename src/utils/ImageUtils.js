// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import sharp from 'sharp';
import slug from 'slug';
import { Teletext } from '@techandsoftware/teletext';
import { ImageToSextants } from '@techandsoftware/image-to-sextants';
import { JSDOM } from 'jsdom';

import { ImageService } from '../services/Image.js';
import { cacheInstance as cache } from '../services/Cache.js';
import log from '../utils/logger.js';
import { FileSaver } from '../services/FileSaver.js';

const imageService = new ImageService();
const dom = new JSDOM('<div id="teletextscreen"></div>');
const teletext = Teletext({
    dom: dom.window
});
teletext.addTo('#teletextscreen');

const CACHE_TIME_SECS = 60*60*24;

const IMAGE_FG_COLOUR = 'cyan';
const IMAGE_POSITION = 'entropy';

export class ImageUtils {
    static async createImageElement(url, titleRows) {
        const cacheKey = getImageCacheKey(url);
        let rows = await cache.retrieve(cacheKey);
        if (rows == null) {
            log.debug('Converting image', url);
            const image = await imageService.get(url);
            const raw = await processImage(image);
            const sextants = new ImageToSextants(raw.data, raw.info.width);
            rows = sextants.getTeletextG1Rows({
                foreground: IMAGE_FG_COLOUR
            });
            cache.store(cacheKey, CACHE_TIME_SECS, rows); // async but we don't wait
        }
        const overlaidRows = [...rows];
        titleRows.forEach((r, index) => {
            overlaidRows[index] = overlayText(overlaidRows[index], r);
        });

        return overlaidRows;
    }

    static getWebpImageName(url) {
        if (url == null) return null;

        const cacheKey = getImageCacheKey(url);
        return `${cacheKey}.webp`;
    }
}

export class ImageConverter {
    constructor(outputOptions) {
        this._saver = new FileSaver(outputOptions);
        this._count = 0;
    }

    get count() {
        return this._count;
    }

    async convertToWebp(url) {
        const cacheKey = getImageCacheKey(url);
        const filename = `${cacheKey}.webp`;

        const exists = await this._saver.exists(filename);
        if (exists) {
            return;
        }

        const rows = await cache.retrieve(cacheKey);
        if (rows == null) {
            log.warn('W59 convertToWebp: image missing', cacheKey);
            return;
        }

        const webp = await renderTeletextToWebp(rows);
        await this._saver.save(filename, webp);
        this._count++;
    }
}

function getImageCacheKey(url) {
    return slug('g1-' + url);
}

async function renderTeletextToWebp(rows) {
    rows.unshift("");
    teletext.setPageRows(rows);
    const svg = Buffer.from(dom.window.document.querySelector('#teletextscreen').innerHTML);
    const webpBuffer = await sharp(svg)
        .webp({
            lossless: true,
        })
        .toBuffer();

    return webpBuffer;
}


function overlayText(source, overlay) {
    let newString = overlay;
    // eslint-disable-next-line no-control-regex
    const res = source.match(/^[\x10-\x1f]+/); // teletext graphic control codes
    if (res) newString += res[0];
    newString += source.slice(newString.length);

    return newString;
}

// resize and dither image
// returns an object with data and info keys
async function processImage(data) {
    const meta = await sharp(data).metadata();

    // 1. squash horizontally so aspect ratio is correct in rendered page
    data = await sharp(data)
        .flatten({
            background: '#000000'
        })
        .extract({
            left: 0,
            top: 0,
            width: meta.width,
            height: Math.round(meta.height * (4/5))
        })
        .resize({
            width: Math.round(meta.width * (8/9)),
            height: Math.round(meta.height * (4/5)),
            fit: 'fill'
        })
        .toBuffer();

    // 2. resize again, then dither to two colours
    data = await sharp(data)
        .resize( {
            width: 39*2,
            height: 69,
            position: IMAGE_POSITION,
            kernel: 'nearest'
        })
        .normalise()
        .sharpen(5)
        .clahe({
            width: 20,
            height: 20,
            maxSlope: 1
        })
        .png({ // conveting to png just to get the dither function
            colours: 2,
            dither: 1.0,
            quality: 0
        })
        .toBuffer();

    // 3. create raw 1-channel image
    const raw = await sharp(data)
        .toColourspace('b-w')
        .normalise()
        .raw().toBuffer({ resolveWithObject: true });

    return raw;
}
