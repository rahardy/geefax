// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

export const Attrs = {
    T: {
        WHITE:   '\x07',
        CYAN:    '\x06',
        MAGENTA: '\x05',
        BLUE:    '\x04',
        YELLOW:  '\x03',
        GREEN:   '\x02',
        RED:     '\x01',
        BLACK:   '\x00',
    },
    G: {
        WHITE:   '\x17',
    },
    SEPERATED:   '\x1a',
    BLACK_BACKGROUND: '\x1c',
    CONTIGUOUS:  '\x19',
    NB:          '\x1d', // new background
};
