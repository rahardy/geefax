// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { Utils } from '../utils/Utils.js';
import { Attrs } from '../Attributes.js';

const COLS = 40;
const LINK_ROW = 24;

export class ComposedPage {
    constructor(master, links) {
        if (master) {
            Object.assign(this, master);
            this._rows = Utils.decodeBase64URLEncoded(this.master);
        } else {
            this._rows = [];
            this.firstRowForContent = 1;
            this.numRowsForContent = 23;
        }
        this._links = links;
        this._currentRowNum = 0;
    }

    goToRow(rowNum) {
        this._currentRowNum = rowNum;
    }

    goToFirstRowForContent() {
        this._currentRowNum = this.firstRowForContent;
    }

    add(element) {
        element.forEach(row => {
            const matched = this._rows[this._currentRowNum].match(/{{.*[:.]}}/);
            // retain the template slot on top of the new row
            if (matched) {
                row = row.slice(0, matched.index) + 
                    matched[0] + 
                    row.slice(matched.index + matched[0].length);
            }
            this._rows[this._currentRowNum] = row;
            this._currentRowNum++;
        });
    }

    get rows() {
        return this._rows;
    }

    createTti() {
        let tti = this._generatePNLine();
        tti += this._generateSCLine();
        tti += this._generatePSLine();
        this._rows.forEach((row, rowNum) => {
            tti += getOutputLine(row, rowNum);
        });
        tti += this._generateFLLine();
        return tti;
    }

    dump() {
        console.log(this._rows.join("\n"));
    }

    setPageNumber(number) {
        this._pageNumber = number;
    }

    setSubPageNumber(number) {
        this._subPageNumber = number;
    }

    fillTemplateValues(values) {
        this._rows = this._rows.map(row => {
            for (const k in values) {
                if (k.match(/[:.]$/)) {
                    // keys ending in : or . are right-aligned and extend over text to the left
                    const regex = `(?<leftPart>.*)(?<templateSlot>{{${k}}})(?<rightPart>.*)`;
                    const re = new RegExp(regex);
                    const matched = row.match(re);
                    if (matched) {
                        const value = values[k].padStart(matched.groups.templateSlot.length, ' ');
                        const leftPartLength = COLS - matched.groups.rightPart.length - value.length;
                        const leftPart = row.slice(0, leftPartLength);
                        row = leftPart + value + matched.groups.rightPart;
                    }
                } else {
                    // otherwise they are left-aligned and expand to fill the length of the template slot
                    const templateSlot = `{{${k}\\s*?}}`;
                    const re = new RegExp(templateSlot, 'g');
                    row = row.replace(re, Utils.replaceAndPad(values[k]));
                }
            }
            return row;
        });
    }

    fillLinkLabels(labels) {
        const items = [];
        if (labels.r != null) items.push(Attrs.T.RED + labels.r);
        if (labels.g != null) items.push(Attrs.T.GREEN + labels.g);
        if (labels.y != null) items.push(Attrs.T.YELLOW + labels.y);
        if (labels.b != null) items.push(Attrs.T.CYAN + labels.b);

        if (items.length == 0)
            return;

        this._rows[LINK_ROW] = Utils.justifyItems(COLS, items);
    }

    _generatePNLine() {
        const subPageNumber = String(this._subPageNumber).padStart(2, 0);
        return `PN,${this._pageNumber}${subPageNumber}\n`;
    }

    _generateSCLine() {
        const subPageNumber = String(this._subPageNumber).padStart(4, 0);
        return `SC,${subPageNumber}\n`;
    }

    _generatePSLine() {
        return "PS,8000\n";
    }

    _generateFLLine() {
        let line = 'FL,';
        ['r', 'g', 'y', 'b'].forEach(colour => {
            line += getPageForLink(this._links[colour]) + ',';
        });
        line += '0,' + getPageForLink(this._links.index) + '\n';
        return line;
    }
}

function getOutputLine(row, rowNum) {
    let line = `OL,${rowNum},`;

    if (row.match(/^\s+$/))
        return line + "\n";

    for (let i = 0; i < row.length; i++) {
        const code = row.charCodeAt(i);
        if (code >= 0 && code < 0x20)
            line += String.fromCharCode(code + 0x80);
        else
            line += row[i];
    }
    line += "\n";
    return line;
}

function getPageForLink(colour) {
    return colour == null ? '0' : colour;
}
