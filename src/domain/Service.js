// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

// Service is a logical structure of a teletext service
// It contains Index or Page objects. The Index pages refer to multiple Page objects
// Both are containers for SubPage, which contains the elements of the subpage
// This doesn't contain the rendered layout
const MAX_NUM_SUBPAGES = 100;

class NoPageError extends Error {}

export class Service {
    constructor() {
        this._pages = {};
    }

    addIndexPage(number, indexObj) {
        this._pages[number] = new Index(this.addPage.bind(this), number, indexObj);
        return this._pages[number];
    }

    addGalleryPage(number) {
        this._pages[number] = new Gallery();
        return this._pages[number];
    }

    addPage(number, page) {
        this._pages[number] = page;
        return this._pages[number];
    }

    dump() {
        console.debug(JSON.stringify(this, null, ' '));
    }

    getPage(number) {
        if (number in this._pages)
            return this._pages[number];

        throw new NoPageError(`E21 getPage: no page with number ${number}`);
    }

    get pages() {
        return Object.keys(this._pages);
    }

    get indexPages() {
        return Object.keys(this._pages).filter(pn => this._pages[pn].isIndex);
    }

    get galleryPages() {
        return Object.keys(this._pages).filter(pn => this._pages[pn].isGallery);
    }

    __test__getSnapshot() {
        const snapshot = {};
        for (const pageNum in this._pages) {
            const page = this._pages[pageNum];
            snapshot[pageNum] = page.composedSubPages.map(composed => composed.createTti());
        }
        return snapshot;
    }

    __test__getLinks() {
        const links = {}
        for (const pageNum in this._pages) {
            const page = this._pages[pageNum];
            links[pageNum] = {
                links: page._links,
                linkLabels: page._linkLabels
            };
        }
        return links;
    }
}

class BasePage {
    constructor(master = null) {
        this._elements = [];
        this._subPages = [];
        this._master = master;
        this._links = {
            index: null,
            r: null,
            g: null,
            y: null,
            b: null
        };
        this._linkLabels = {
            r: null,
            g: null,
            y: null,
            b: null
        };
        this._templateValues = {};
    }

    get isIndex() {
        return this.constructor.name == 'Index';
    }

    get isGallery() {
        return this.constructor.name == 'Gallery';
    }

    set master(master) {
        this._master = master;
    }

    get master() {
        return this._master;
    }

    addElement(element, linePrefix) {
        if (typeof linePrefix == 'string')
            element = element.map(line => linePrefix + line);

        this._elements.push(element);
    }

    createSubPages(availableRowsPerSubpage, blankRowBetweenElements = false) {
        let subPage = null;

        for (let e = 0; e < this._elements.length; e++) {
            const element = this._elements[e];
            const rowsNeeded = element.length;
            if (subPage == null) subPage = new SubPage(availableRowsPerSubpage);
            if (subPage.availableRows >= rowsNeeded) {
                subPage.addElement(element);
            } else if (e == 1 || rowsNeeded > availableRowsPerSubpage) {
                // split this element as it's too big for a subpage, or if only one element (probably the headline) is on the subpage
                const splitElements = splitElement(subPage.availableRows, availableRowsPerSubpage, element);

                for (let s = 0; s < splitElements.length; s++) {
                    const rowsNeeded = splitElements[s].length;
                    if (subPage.availableRows >= rowsNeeded) {
                        subPage.addElement(splitElements[s]);
                    } else {
                        this._subPages.push(subPage);
                        subPage = new SubPage(availableRowsPerSubpage);
                        subPage.addElement(splitElements[s]);
                    }
                }
            } else {
                this._subPages.push(subPage);
                subPage = new SubPage(availableRowsPerSubpage);
                subPage.addElement(element);
            }
            // blank row added apart from after last element or if no available rows
            if (blankRowBetweenElements && e < this._elements.length - 1 && subPage.availableRows >= 1)
                subPage.addElement([""]);

            if (this._subPages.length == MAX_NUM_SUBPAGES)
                break;
        }
        if (subPage != null && this._subPages.length < MAX_NUM_SUBPAGES)
            this._subPages.push(subPage);
    }

    setMaxNumSubPages(maxNumSubPages) {
        this._subPages = this._subPages.slice(0, maxNumSubPages);
    }

    get numSubPages() {
        return this._subPages.length;
    }

    get subPages() {
        return this._subPages;
    }

    get composedSubPages() {
        return this._subPages.map(sp => sp.composedPage);
    }

    setLinks(index, r, g, y, b) {
        this._links = {
            index,
            r,
            g,
            y,
            b
        };
    }

    setLinkLabels(r, g, y, b) {
        this._linkLabels = {
            r, g, y, b
        };
    }

    set templateValues(values) {
        this._templateValues = values;
    }

    get templateValues() {
        return this._templateValues;
    }

    get links() {
        return this._links;
    }

    get linkLabels() {
        return this._linkLabels;
    }

    set index(i) {
        this._links.index = i;
    }

    set red(r) {
        this._links.r = r;
    }
    set redLabel(r) {
        this._linkLabels.r = r;
    }

    set green(g) {
        this._links.g = g;
    }
    set greenLabel(g) {
        this._linkLabels.g = g;
    }

    set yellow(y) {
        this._links.y = y;
    }
    set yellowLabel(y) {
        this._linkLabels.y = y;
    }

    set blue(b) {
        this._links.b = b;
    }
    set blueLabel(b) {
        this._linkLabels.b = b;
    }

    set indexLink(i) {
        this._links.index = i;
    }
}

function splitElement(availableRowsOnFirstSubPage, availableRowsOnSubPage, element) {
    const splitElements = [];

    let newElement = [];
    for (let row = 0; row < availableRowsOnFirstSubPage; row++) {
        newElement.push(element.shift());
    }
    if (newElement.length) splitElements.push(newElement);

    while (element.length) {
        newElement = [];
        for (let row = 0; row < availableRowsOnSubPage; row++) {
            if (!element.length)
                break;
            newElement.push(element.shift());
        }
        if (newElement.length) splitElements.push(newElement);
    }

    return splitElements;
}

class Index extends BasePage {
    constructor(addPageToServiceFn, pageNum, indexObj) {
        super();
        this._addPageToService = addPageToServiceFn;
        this._childPages = {};
        this._childCount = 0;
        this._pageNumbers = [];
        this._indexPageNum = parseInt(pageNum);
        Object.assign(this, indexObj);
    }

    addStory(story) {
        this._childCount++;
        this._childPages[this._childCount] = new Page(story, this._getRemoveFromIndexFn(this._childCount));
    }

    assignPageNumbersToStories() {
        let pageNum = this._indexPageNum;
        Object.keys(this._childPages).forEach(id => {
            pageNum++;
            this._pageNumbers.push(pageNum);
            this._addPageToService(pageNum, this._childPages[id]);
        });
    }

    get childPages() {
        return Object.values(this._childPages);
    }

    get pageNumbersInIndex() {
        return this._pageNumbers;
    }

    // function passed to child pages so they can remove from this index
    _getRemoveFromIndexFn(id) {
        return () => delete this._childPages[id];
    }
}

class Gallery extends BasePage {
    constructor() {
        super();
        this._stories = [];
    }

    addStory(story, params) {
        this._stories.push({
            title: story.title,
            imageUrl: story.imageUrl
        });
        Object.assign(this, params);
    }

    createSubPages(availableRowsPerSubpage) {
        this._elements.forEach(el => {
            const subPage = new SubPage(availableRowsPerSubpage);
            subPage.addElement(el);
            this._subPages.push(subPage);
        });
    }

    get stories() {
        return this._stories;
    }
}

class Page extends BasePage {
    constructor(pageObj, removeFromIndexFn) {
        super();
        this._removeFromIndex = removeFromIndexFn;
        Object.assign(this, pageObj);
    }

    removeFromIndex() {
        this._removeFromIndex();
    }
}

class SubPage {
    constructor(availableRows) {
        this._availableRows = availableRows;
        this._elements = [];
    }

    get availableRows() {
        return this._availableRows;
    }

    addElement(element) {
        this._elements.push(element);
        this._availableRows -= element.length;
    }

    get elements() {
        return this._elements;
    }

    addComposedPage(composed) {
        this._composed = composed;
    }

    get composedPage() {
        return this._composed;
    }
}

export const __testExports = {
    BasePage
};
