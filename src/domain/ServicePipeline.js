// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

// ServicePipeline is a pipeline of steps from populating the Service with content to creating a ComposedPage per SubPage, which is the rendered teletext frame

import { ComposedPage } from './ComposedPage.js';
import { Utils } from '../utils/Utils.js';
import { ImageConverter, ImageUtils } from '../utils/ImageUtils.js'
import log from '../utils/logger.js';

import fs from 'fs/promises';
import path from 'path';
import wordwrap from 'wordwrapjs';
import { Attrs } from '../Attributes.js';

const ROW_LENGTH = 40;
const PADDING = '  .';
const HEADLINE_LENGTH = 35;

class NoContentError extends Error {}

export class ServicePipeline {
    constructor(service, styling) {
        this._service = service;
        this._styling = styling;
    }

    // 1.Create the logical Service structure
    async populateService() {
        log.debug('populateService');
        // log.group();

        // a) populate story pages first in case there's a problem with the content
        const indexPageNums = this._service.indexPages;
        const promises = [];
        indexPageNums.forEach(number => {
            const indexPage = this._service.getPage(number);
            indexPage.childPages.forEach(page => {
                promises.push(this._createStoryPageElements(number, page));
            });
        });
        const promiseResults = await Promise.allSettled(promises);
        const fulfilledCount = promiseResults.filter(r => r.status == 'fulfilled').length;
        log.info('populateService:', fulfilledCount, 'pages populated');

        promiseResults.filter(r => r.status == 'rejected').forEach(r => {
            log.warn('W45 populateService:', r.reason)
        });

        // b) populate index pages
        indexPageNums.forEach(number => {
            const page = this._service.getPage(number);
            page.assignPageNumbersToStories();
            this._createIndexPageElements(page);
        });
        // log.groupEnd();

        // c) populate gallery pages
        const galleryPageNums = this._service.galleryPages;
        const galleryPromises = [];
        galleryPageNums.forEach(number => {
            const page = this._service.getPage(number);
            galleryPromises.push(this._createGalleryPageElements(page));
        });
        await Promise.allSettled(galleryPromises);

        return fulfilledCount;
    }

    // 2. links are the colour button nav. This creates the logical linkage
    createLinks() {
        log.debug('createLinks');
        const numbers = this._service.pages;
        numbers.forEach(number => {
            const page = this._service.getPage(number);
            if (page.isIndex) {
                const indexedPages = page.pageNumbersInIndex;
                page.red = indexedPages[0];
                indexedPages.forEach((childPageNumber, cpi) => {
                    const childPage = this._service.getPage(childPageNumber);
                    childPage.indexLink = number;
                    if (cpi == indexedPages.length - 1) {
                        childPage.red = indexedPages[0];
                        childPage.redLabel = "First story";
                    } else {
                        childPage.red = indexedPages[cpi + 1];
                        childPage.redLabel = "Next story";
                    }
                });
            } else {
                // noop
            }
        });
    }

    // 3. Each Page has 1 or more SubPages
    createSubPages(maxNumSubPages) {
        log.debug('createSubPages');
        // log.group();
        const numbers = this._service.pages;
        numbers.forEach(number => {
            // log.debug(number);
            const page = this._service.getPage(number);
            if (page.isIndex) {
                page.createSubPages(page.master.numRowsForContent);
            } else if (page.isGallery) {
                page.createSubPages(page.master.numRowsForContent);
            } else {
                page.createSubPages(page.master.numRowsForContent, true);
                page.setMaxNumSubPages(maxNumSubPages);
            }
        });
        // log.groupEnd();
    }

    // 4. For every SubPage, creating the rendered teletext frame, which is represented by ComposedPage
    layoutPages() {
        log.debug('layoutPages');
        const numbers = this._service.pages;
        numbers.forEach(number => {
            const page = this._service.getPage(number);
            const subPages = page.subPages;
            subPages.forEach(subPage => {
                const composedPage = new ComposedPage(page.master, page.links);
                composedPage.goToFirstRowForContent();
                const elements = subPage.elements;
                elements.forEach(element => {
                    composedPage.add(element);
                });
                subPage.addComposedPage(composedPage);
            });
        });
    }

    // 5. Fill the on-screen subpage numbers, template slots and colour link labels
    // template slots are things like section headings
    finaliseComposedPages() {
        log.debug('finaliseComposedPages');
        const numbers = this._service.pages;
        numbers.forEach(number => {
            const page = this._service.getPage(number);
            const composedSubPages = page.composedSubPages;
            composedSubPages.forEach((sp, index) => {
                sp.setPageNumber(number);
                const currentSubPageNumber = composedSubPages.length == 1 ? 0 : index + 1;
                sp.setSubPageNumber(currentSubPageNumber);
                const subPageLabels = getSubPageLabels(currentSubPageNumber, composedSubPages.length);
                page.templateValues['spn:'] = subPageLabels.numberingWithBullets;
                page.templateValues['.'] = subPageLabels.numberingWithoutBullets;

                sp.fillTemplateValues(page.templateValues);
                sp.fillLinkLabels(page.linkLabels);
            });
        });
    }

    // 6. Output to .tti files
    // Format from https://zxnet.co.uk/teletext/documents/ttiformat.pdf
    // The WL line (for weblink) is an addition to contain a URL for the source content
    // The WI link (for webimage) is an addition to contain a webp image used for meta tags in the containing web page
    // The encoding is 'latin1', which is actually to get byte output not utf8 so that the teletext control codes bytes are preserved. Not to be confused with the 7-bit teletext set which is represented in the OL lines within the .tti
    // returns a promise that resolves when all files written
    createTtiFiles(outputDirectory) {
        log.debug('createTtiFiles');
        // log.group();
        const numbers = this._service.pages;
        const promises = [];
        numbers.forEach(number => {
            const page = this._service.getPage(number);
            let tti = '';
            if (page.webUrl) tti += `WL,${page.webUrl}\n`;
            if (page.webpImage) tti += `WI,${page.webpImage}\n`;
            const composedSubPages = page.composedSubPages;
            composedSubPages.forEach(sp => {
                tti += sp.createTti();
            });
            const promise = fs.writeFile(path.join(outputDirectory, `P${number}.tti`), tti, {
                encoding: 'latin1'
            });
            promises.push(promise);
        });

        return Promise.all(promises);
        // log.groupEnd();
    }

    // 7. Save webp images from the teletext mosaic gallery images
    // returns a Promise that resolves when all files written
    async createImageFiles(outputOptions) {
        log.debug('createImageFiles');
        const numbers = this._service.pages;
        const promises = [];
        const converter = new ImageConverter(outputOptions);
        numbers.forEach(number => {
            const page = this._service.getPage(number);
            if (page.imageUrl) {
                promises.push(converter.convertToWebp(page.imageUrl));
            }
        });
        await Promise.all(promises);
        log.debug(`Created ${converter.count} .webp images`);
    }

    _createIndexPageElements(page) {
        const numbers = page.pageNumbersInIndex;
        const pageNumColour = this._styling.indexPageNumberColour;
        numbers.forEach((pageNum, storyNum) => {
            const childPage = this._service.getPage(pageNum);
            const headlineParts = wrapper(childPage.title, HEADLINE_LENGTH);
            const element = [];
            headlineParts.forEach((part, index) => {
                if (index == headlineParts.length - 1)
                element.push(this._headlineRow(storyNum, part, `${pageNumColour}${pageNum}`));
                else {
                    const justified = Utils.justifyItems(HEADLINE_LENGTH, part.split(' '));
                    element.push(this._headlineRow(storyNum, justified));
                }
            });
            page.addElement(element);
        });
    }

    async _createStoryPageElements(indexPageNumber, page) {
        const paragraphs = await page.getParagraphs();
        if (paragraphs.length == 0) {
            page.removeFromIndex();
            throw new NoContentError(`Story removed in index page ${indexPageNumber}: no paragraphs in story id ${page.id}`);
        }

        page.addElement(wrapper(page.title, 39), this._styling.storyHeadlineColour);

        paragraphs.forEach(p => {
            const element = wrapper(p, 39);
            page.addElement(element, ' ');
        });
        return true;
    }

    async _createGalleryPageElements(page) {
        const promises = [];
        const titleWrapLength = ROW_LENGTH - this._styling.galleryHeadlineColour.length;
        page.stories.forEach(story => {
            if (story.imageUrl) {
                let titleParts = wrapper(story.title, titleWrapLength);
                titleParts = colouriseText(titleParts, this._styling.galleryHeadlineColour);
                promises.push(ImageUtils.createImageElement(story.imageUrl, titleParts));
            }
        });
        const promiseResults = await Promise.allSettled(promises);
        const failed = promiseResults.filter(p => p.status == 'rejected');
        if (failed.length)
            log.warn('W224 _createGalleryPageElements: failed to create', failed.length, 'images');
        failed.forEach(p => log.warn(p.reason));

        promiseResults.filter(p => p.status == 'fulfilled').forEach(p => {
            page.addElement(p.value);
        });
        return true;
    }

    _headlineRow(index, headline, pageNum) {
        const colour = index % 2
        ? this._styling.indexHeadlineColour.even
        : this._styling.indexHeadlineColour.odd;

        if (typeof pageNum != 'undefined') {
            const gapLength = ROW_LENGTH - 1 - headline.length - pageNum.length;
            if (gapLength < PADDING.length) {
                headline = Utils.justifyItems(HEADLINE_LENGTH, headline.split(' '));
            } else {
                const headlinePadLength = gapLength % PADDING.length;
                headline = headline.padEnd(headline.length + headlinePadLength, ' ');
                pageNum = pageNum.padStart(ROW_LENGTH - 1 - headline.length, PADDING);
            }

            return `${colour}${headline}${pageNum}`;
        }
        return `${colour}${headline}`;
    }
}

function colouriseText(textParts, attributes) {
    return textParts.map(p => {
        let newPart = attributes + p;
        if (newPart.length < 39)
            newPart = newPart + ' ' + Attrs.BLACK_BACKGROUND;
        return newPart;
    });
}

function wrapper(text, length) {
    const wrapped = wordwrap.wrap(text, { width: length });
    const substrings = wrapped.split("\n");
    return substrings;
}


function getSubPageLabels(subPageNumber, totalSubPages) {
    if (totalSubPages == 1) {
        return {
            numberingWithBullets: '',
            numberingWithoutBullets: ''
        }
    }

    const numberingWithoutBullets = `${subPageNumber}/${totalSubPages}`;
    const lenTotalSubPages = String(totalSubPages).length;
    const paddedNumberingWithoutBullets = `${String(subPageNumber).padStart(lenTotalSubPages, ' ')}/${totalSubPages}`;

    const bullets = makeSubPageBullets(subPageNumber, totalSubPages);
    const numberingWithBullets = bullets + paddedNumberingWithoutBullets;

    return {
        numberingWithBullets: (numberingWithBullets.length < ROW_LENGTH)
            ? numberingWithBullets
            : numberingWithoutBullets,
        numberingWithoutBullets: Attrs.T.WHITE + numberingWithoutBullets
    }
}

function makeSubPageBullets(currentSubPage, totalSubPages) {
    const CURRENT_SUBPAGE_BULLET = '\x2f';  // Teletext g2 charset mosaic
    const OTHER_SUBPAGE_BULLET = '\x24';    // Teletext g2 charset mosaic

    let bullets;
    for (let s = 1; s <= totalSubPages; s++) {
        if (s == 1) {
            if (s == currentSubPage)
                bullets = Attrs.G.WHITE + CURRENT_SUBPAGE_BULLET + Attrs.SEPERATED;
            else
                bullets = Attrs.G.WHITE + Attrs.SEPERATED + OTHER_SUBPAGE_BULLET + ' ';
        } else {
            if (s == currentSubPage) {
                bullets = bullets.trim();
                bullets += Attrs.CONTIGUOUS + CURRENT_SUBPAGE_BULLET + Attrs.SEPERATED;
            } else
                bullets += OTHER_SUBPAGE_BULLET + ' ';
        }
    }
    bullets = bullets.slice(0, bullets.length - 1);
    bullets += Attrs.T.WHITE;
    return bullets;
}
