// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { Utils } from '../utils/Utils.js';
import { ImageUtils } from '../utils/ImageUtils.js';
import { cacheInstance as cache } from '../services/Cache.js';
import log from '../utils/logger.js';

const CACHE_TIME_SECS = 60*60*24;

export class Story {
    constructor(params) {
        const story = params.story;

        this._contentFn = params.contentFn;
        this._charset = params.charset;
        this._cacheKey = `${params.charset}-${params.story.id}`;

        this.title = Utils.mapChars(story.webTitle, params.charset);
        this.date = new Date(story.webPublicationDate);
        this.id = story.id;
        this.webUrl = story.webUrl;
        this.imageUrl = story.fields?.thumbnail ?? null;
        this.webpImage = ImageUtils.getWebpImageName(this.imageUrl);
        this.getParagraphs = this._getParagraphs;

        Object.assign(this, params.params);
    }

    async _getParagraphs() {
        let res = await cache.retrieve(this._cacheKey);
        if (res != null) {
            log.debug('Story.getParagraphs: cache-hit key ', this._cacheKey);
            return res;
        }
    
        res = await this._contentFn();
        let paragraphs = Utils.convertHTMLToParagraphs(res.body);
        paragraphs = paragraphs.map(p => Utils.mapChars(p, this._charset));
        cache.store(this._cacheKey, CACHE_TIME_SECS, paragraphs); // async but we don't wait
    
        return paragraphs;
    }
}
