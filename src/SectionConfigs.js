// SPDX-FileCopyrightText: 2023 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { PAGE_MASTERS } from './PageMasters.js';
import { Attrs } from './Attributes.js';

const indexNav =  {
    uk: {
        nav1: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'In pictures' + Attrs.T.RED + '130  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'USA          ' + Attrs.T.RED + '200',
        nav2: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'Australia  ' + Attrs.T.RED + '300  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'International' + Attrs.T.RED + '400',
    },
    us: {
        nav1: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'In pictures' + Attrs.T.RED + '230  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'UK           ' + Attrs.T.RED + '100',
        nav2: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'Australia  ' + Attrs.T.RED + '300  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'International' + Attrs.T.RED + '400',
    },
    au: {
        nav1: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'In pictures' + Attrs.T.RED + '330  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'UK           ' + Attrs.T.RED + '100',
        nav2: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'USA        ' + Attrs.T.RED + '200  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'International' + Attrs.T.RED + '400',
    },
    int: {
        nav1: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'In pictures' + Attrs.T.RED + '430  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'UK           ' + Attrs.T.RED + '100',
        nav2: Attrs.NB + Attrs.T.BLUE + Attrs.T.BLACK + 'USA        ' + Attrs.T.RED + '200  ' + Attrs.T.BLUE + Attrs.T.BLACK + 'Australia    ' + Attrs.T.RED + '300',
    }
};

export const STYLE_CONFIG = {
    indexPageNumberColour: Attrs.T.YELLOW,
    indexHeadlineColour: {
        even: Attrs.T.WHITE,
        odd: Attrs.T.CYAN
    },
    storyHeadlineColour: Attrs.T.CYAN,
    galleryHeadlineColour: ' ' + Attrs.NB + Attrs.T.BLACK
};


// apiId should be uk, us, au to get edition front pages, but the Guardian API is broken right now
export const SECTIONS = {
    uk: {
        apiId: 'uk-news',
        index: {
            number: '100',
            params: {
                master: PAGE_MASTERS.TOP_STORY_INDEX,
                redLabel: 'First story',
                green: '130',
                greenLabel: 'In pictures',
                yellow: '200',
                yellowLabel: 'USA',
                blue: '300',
                blueLabel: 'Australia',
                templateValues: {
                    edn: 'UK',
                    nav1: indexNav.uk.nav1,
                    nav2: indexNav.uk.nav2,
                }
            }
        },
        gallery: {
            number: '130',
            params: {
                master: PAGE_MASTERS.GALLERY,
                index: '100',
                red: '100',
                redLabel: 'UK',
                green: '230',
                greenLabel: 'USA',
                yellow: '330',
                yellowLabel: 'Australia',
                blue: '430',
                blueLabel: 'International',
            }
        },
        storyParams: {
            master: PAGE_MASTERS.TOP_STORY,
            templateValues: {
                edn: 'UK',
                nav: Attrs.NB + Attrs.T.BLACK + 'UK' + Attrs.T.RED + '100    ' + Attrs.T.BLACK + 'USA' + Attrs.T.RED + '200    ' + Attrs.T.BLACK + 'Australia' + Attrs.T.RED + '300'
            },
            green: '130',
            greenLabel: 'In pictures',
            yellow: '200',
            yellowLabel: 'USA',
            blue: '300',
            blueLabel: 'Australia'
        }
    },
    us: {
        apiId: 'us-news',
        index: {
            number: '200',
            params: {
                master: PAGE_MASTERS.TOP_STORY_INDEX,
                redLabel: 'First story',
                index: '100',
                green: '230',
                greenLabel: 'In pictures',
                yellow: '100',
                yellowLabel: 'UK',
                blue: '300',
                blueLabel: 'Australia',
                templateValues: {
                    edn: 'USA',
                    nav1: indexNav.us.nav1,
                    nav2: indexNav.us.nav2,
                }
            }
        },
        gallery: {
            number: '230',
            params: {
                master: PAGE_MASTERS.GALLERY,
                index: '200',
                red: '130',
                redLabel: 'UK',
                green: '200',
                greenLabel: 'USA',
                yellow: '330',
                yellowLabel: 'Australia',
                blue: '430',
                blueLabel: 'International',
            }
        },
        storyParams: {
            master: PAGE_MASTERS.TOP_STORY,
            templateValues: {
                edn: 'USA',
                nav: Attrs.NB + Attrs.T.BLACK + 'UK' + Attrs.T.RED + '100    ' + Attrs.T.BLACK + 'USA' + Attrs.T.RED + '200    ' + Attrs.T.BLACK + 'Australia' + Attrs.T.RED + '300'
            },
            green: '230',
            greenLabel: 'In pictures',
            yellow: '100',
            yellowLabel: 'UK',
            blue: '300',
            blueLabel: 'Australia'
        }
    },
    au: {
        apiId: 'australia-news',
        index: {
            number: '300',
            params: {
                master: PAGE_MASTERS.TOP_STORY_INDEX,
                redLabel: 'First story',
                index: '100',
                green: '330',
                greenLabel: 'In pictures',
                yellow: '100',
                yellowLabel: 'UK',
                blue: '200',
                blueLabel: 'USA',
                templateValues: {
                    edn: 'Austr\'la',
                    nav1: indexNav.au.nav1,
                    nav2: indexNav.au.nav2,
                }
            }
        },
        gallery: {
            number: '330',
            params: {
                master: PAGE_MASTERS.GALLERY,
                index: '300',
                red: '130',
                redLabel: 'UK',
                green: '230',
                greenLabel: 'USA',
                yellow: '300',
                yellowLabel: 'Australia',
                blue: '430',
                blueLabel: 'International',
            }
        },
        storyParams: {
            master: PAGE_MASTERS.TOP_STORY,
            templateValues: {
                edn: 'Australia',
                nav: Attrs.NB + Attrs.T.BLACK + 'UK' + Attrs.T.RED + '100    ' + Attrs.T.BLACK + 'USA' + Attrs.T.RED + '200    ' + Attrs.T.BLACK + 'Australia' + Attrs.T.RED + '300'
            },
            green: '330',
            greenLabel: 'In pictures',
            yellow: '100',
            yellowLabel: 'UK',
            blue: '200',
            blueLabel: 'USA'
        }
    },
    // international: {
    //     apiId: 'international',
    //     index: {
    //         number: '400',
    //         params: {
    //             master: PAGE_MASTERS.TOP_STORY_INDEX,
    //             redLabel: 'First story',
    //             index: '100',
    //             green: '430',
    //             greenLabel: 'In pictures',
    //             yellow: '100',
    //             yellowLabel: 'UK',
    //             blue: '200',
    //             blueLabel: 'USA',
    //             templateValues: {
    //                 edn: 'Int\'nal',
    //                 nav1: indexNav.int.nav1,
    //                 nav2: indexNav.int.nav2,
    //             }
    //         }
    //     },
    //     gallery: {
    //         number: '430',
    //         params: {
    //             master: PAGE_MASTERS.GALLERY,
    //             index: '400',
    //             red: '130',
    //             redLabel: 'UK',
    //             green: '230',
    //             greenLabel: 'USA',
    //             yellow: '330',
    //             yellowLabel: 'Australia',
    //             blue: '400',
    //             blueLabel: 'International',
    //         }
    //     },
    //     storyParams: {
    //         master: PAGE_MASTERS.TOP_STORY,
    //         templateValues: {
    //             edn: 'International',
    //             nav: Attrs.NB + Attrs.T.BLACK + 'International' + Attrs.T.RED + '400  ' + Attrs.T.BLACK + 'UK' + Attrs.T.RED + '100 ' + Attrs.T.BLACK + 'USA' + Attrs.T.RED + '200'
    //         },
    //         green: '430',
    //         greenLabel: 'In pictures',
    //         yellow: '100',
    //         yellowLabel: 'UK',
    //         blue: '200',
    //         blueLabel: 'USA'
    //     }
    // }
};
