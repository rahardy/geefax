// SPDX-FileCopyrightText: 2023 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { Service } from './domain/Service.js';
import { ServicePipeline } from './domain/ServicePipeline.js';
import { Story } from './domain/Story.js';
import { Guardian } from './services/Guardian.js';
import { Utils } from './utils/Utils.js';
import { tti2json } from './utils/tti2json.js';
import log from './utils/logger.js';

import { SECTIONS, STYLE_CONFIG } from './SectionConfigs.js';
const MAX_NUM_SUBPAGES = 255;
const CHARSET = 'g0_latin__english';

if (!('GUARDIAN_API_KEY' in process.env))
    throw new Error('E13 Environment var GUARDIAN_API_KEY must be set');
const guardian = new Guardian(process.env.GUARDIAN_API_KEY);

async function addSection(service, s) {
    const index = service.addIndexPage(s.index.number, s.index.params);
    const gallery = s.gallery ? service.addGalleryPage(s.gallery.number) : null;

    try {
        // const stories = await guardian.picksById(s.apiId); // edition front-pages - API issues right now
        const stories = await guardian.newsById(s.apiId);
        stories.forEach(story => {
            const storyObj = new Story({
                story: story,
                params: s.storyParams,
                contentFn: () => guardian.item(story.id, story.apiUrl),
                charset: CHARSET
            });
            index.addStory(storyObj);
            if (gallery) gallery.addStory(storyObj, s.gallery.params);
        });
    } catch (e) {
        log.error(e);
        throw Error(`E31 addSection for section apiId ${s.apiId}: Failed to get stories`);
    }
}

async function createService(sections) {
    const service = new Service();
    // await addSection(service, sections.uk);
    for (const section of Object.keys(sections)) {
        await addSection(service, sections[section]);
    }
    return service;
}

async function runPipeline(sections) {
    let pipeline;
    try {
        const service = await createService(sections);
        pipeline = new ServicePipeline(service, STYLE_CONFIG);
        await pipeline.populateService();
        pipeline.createLinks();
        pipeline.createSubPages(MAX_NUM_SUBPAGES);
        pipeline.layoutPages();
        pipeline.finaliseComposedPages();
    } catch (e) {
        log.error('E62 runPipeline failed:');
        log.error(e);
        process.exitCode = 1;
        throw new Error('E62 runPipeline failed');
    }
    return pipeline;
}

async function generateFiles(pipeline) {
    try {
        const ttiOutputDir = await Utils.createEmptyDir('geefax-tti');
        await pipeline.createTtiFiles(ttiOutputDir);

        const outputOptions = {};
        if (process.env.OUTPUT_BUCKET_NAME) {
            outputOptions.bucket = process.env.OUTPUT_BUCKET_NAME;
        } else {
            const jsonOutputDir = await Utils.createEmptyDir('geefax-out');
            outputOptions.dir = jsonOutputDir;
        }

        await tti2json(ttiOutputDir, outputOptions);
        await pipeline.createImageFiles(outputOptions);
    } catch (e) {
        log.error('E85 generateFiles failed:');
        log.error(e);
        process.exitCode = 2;
        throw new Error('E85 generateFiles failed');
    }
}

function prechecks() {
    if (process.env.NODE_ENV == 'production' &&
        !('AWS_REGION' in process.env))
        log.warn('W77 Environment var AWS_REGION should be set in production environment');
}

// eslint-disable-next-line no-unused-vars
export async function handler(event, context) {
    prechecks();
    guardian.resetCounter();
    const pipeline = await runPipeline(SECTIONS);
    await generateFiles(pipeline);
    const counts = guardian.counter;
    log.info(`Guardian service GETs: ${counts.gets}  Cache hits: ${counts.cacheHits}`);
}

export const __testExports = {
    runPipeline
};

// TODO

// remove 'in pictures'
// quiz rendering?
