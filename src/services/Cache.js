// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { FileCache } from './FileCache.js';
import { S3Cache } from './S3Cache.js';
import { NullCache } from './NullCache.js';

import log from '../utils/logger.js';

export class Cache {
    constructor(prefix) {
        if (process.env.NODE_ENV == 'test') {
            this._cache = new NullCache();
        } else if (process.env.CACHE_BUCKET_NAME) {
            log.debug(`> Cache: using s3 cache`);
            this._cache = new S3Cache(process.env.CACHE_BUCKET_NAME);
        } else {
            log.debug(`> Cache: using file cache`);
            this._cache = new FileCache(`${prefix}-cache`);
        }
    }

    async retrieve(id) {
        return await this._cache.retrieveFromCache(id);
    }

    async store(id, expirySecs, obj) {
        return await this._cache.storeToCache(id, expirySecs, obj);
    }
}

export const cacheInstance = new Cache('geefax');
