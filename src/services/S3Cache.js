// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import s3 from './S3.js';
import log from '../utils/logger.js';

export class S3Cache {
    constructor(bucket) {
        this._bucket = bucket;
    }

    async retrieveFromCache(id) {
        try {
            const res = await s3.getObject({
                Bucket: this._bucket,
                Key: id
            }).promise();
            if (res.Expires < Date.now()) {
                log.debug('cached item expired', res.Expires);
                return null;
            }
            return JSON.parse(res.Body);
        } catch (err) {
            if ('code' in err && err.code == 'NoSuchKey') {
                return null;
            } else {
                log.error('E24 S3Cache.retrieveFromCache exception:');
                log.error(err);
                throw new Error("E24 S3Cache.retrieveFromCache exception");
            }
        }
    }

    async storeToCache(id, expirySecs, obj) {
        const expiryDate = new Date(Date.now() + expirySecs * 1000);
        try {
            await s3.putObject({
                Body: JSON.stringify(obj),
                Bucket: this._bucket,
                Key: id,
                Expires: expiryDate
            }).promise();
        } catch (e) {
            log.error('E41 S3Cache.storeToCache exception:');
            log.error(e);
            throw new Error('E41 S3Cache.storeToCache exception');
        }
    }
}
