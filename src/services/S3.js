// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import AWS from 'aws-sdk';

// s3 singleton
export default new AWS.S3();
