// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

export class NullCache {
    constructor() {}

    retrieveFromCache() {
        return Promise.resolve(null);
    }

    storeToCache() {}
}
