// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import log from '../utils/logger.js';

import os from 'os';
import path from 'path';
import fs from 'fs/promises';

export class FileCache {
    constructor(name) {
        this._cacheDir = path.join(os.tmpdir(), name);
        this._initted = this._init();
    }

    async _init() {
        try {
            await fs.stat(this._cacheDir);
        } catch (e) {
            try {
                await fs.mkdir(this._cacheDir);
            } catch (e) {
                throw new Error('FileCache: failed to create cache dir');
            }
        }
        return true;
    }

    async retrieveFromCache(id) {
        await this._initted;
        const itemPath = this._getCacheItemPath(id);
        try {
            const file = await fs.readFile(itemPath, 'utf8');
            const item = JSON.parse(file);
            const expirationDate = new Date(item.expiry);
            if (expirationDate < Date.now()) {
                log.debug('cached item expired', expirationDate);
                return null;
            }
            return item.obj;
        } catch (e) {
            return null;
        }
    }

    async storeToCache(id, expirySecs, obj) {
        await this._initted;
        const itemPath = this._getCacheItemPath(id);
        // log.debug('_storeToCache storing item to cache:', itemPath);
        const expiryDate = new Date(Date.now() + expirySecs * 1000);
        const cacheItem = {
            expiry: expiryDate,
            obj: obj
        };
        await fs.writeFile(itemPath, JSON.stringify(cacheItem));
    }

    _getCacheItemPath(id) {
        const re = new RegExp('/', 'g');
        id = id.replace(re, '-');
        return path.join(this._cacheDir, id);
    }
}