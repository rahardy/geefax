// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import fs from 'fs/promises';
import path from 'path';

import { Utils } from '../utils/Utils.js';
import log from '../utils/logger.js';

export class FileSaver {
    constructor(outputOptions) {
        if (outputOptions.bucket)
            this._saver = new SaveToS3(outputOptions.bucket);
        else if (outputOptions.dir) 
            this._saver = new SaveToFS(outputOptions.dir);
        else
            throw new Error('E17 FileSaver: bad outputOptions - expected bucket or dir');
    }

    exists(filename) {
        return this._saver.exists(filename);
    }

    save(filename, data, expiryDate) {
        this._saver.save(filename, data, expiryDate);
    }

    get log() {
        return this._saver.log;
    }
}

class SaveToS3 {
    constructor(bucket) {
        this._bucket = bucket;
    }

    async exists(filename) {
        try {
            return await Utils.s3ObjectExists(filename, this._bucket);
        } catch (e) {
            log.error(`E42 SaveToS3.exists: failed to check for ${filename} in bucket ${this._bucket}`);
            log.error(e);
            throw new Error('SaveToS3.exists failed to check for file in s3');
        }
    }

    async save(filename, data, expiryDate) {
        try {
            await Utils.copyFileToS3(filename, data, this._bucket, expiryDate);
        } catch (e) {
            log.error(`E38 SaveToS3.save: failed to write ${filename} to bucket ${this._bucket}:`);
            log.error(e);
            throw new Error('SaveToS3.save failed to write file to s3');
        }
    }

    get log() {
        return `saved files to bucket ${this._bucket}`;
    }
}

class SaveToFS {
    constructor(dir) {
        this._dir = dir;
    }

    async exists() {
        return Promise.resolve(false);
    }

    // eslint-disable-next-line no-unused-vars
    async save(filename, data, expiryDate) {
        const fileName = path.join(this._dir, filename);
        await fs.writeFile(fileName, data);
    }

    get log() {
        return `saved files to dir ${this._dir}`;
    }
}
