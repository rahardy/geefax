// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import https from 'https';

class ClientSideHTTPError extends Error {}
class ServerSideHTTPError extends Error {}

export class ServiceBase {

    constructor() {
        this._httpOptions = {
            agent: new https.Agent({keepAlive: true })
        };
    }

    async get(url, options = {}) {
        // console.debug(`Fetching ${url}`);
        let rawData = [];
        return new Promise((resolve, reject) => {
            const request = https.get(url, this._httpOptions, res => {
    
                res.on('data', (chunk) => {
                    rawData.push(chunk);
                });
    
                res.on('end', () => {
                    if (res.statusCode >= 400 && res.statusCode < 500) {
                        reject(new ClientSideHTTPError(`${this.constructor.name} failed to retrieve url: ${url} : HTTP error returned: ${res.statusCode}`));
                        return;
                    } else if (res.statusCode >= 500) {
                        // log.debug(res.headers);
                        reject(new ServerSideHTTPError(`${this.constructor.name} failed to retrieve url: ${url} : HTTP error returned: ${res.statusCode}`));
                        return;
                    }
                    // log.debug(`${this.constructor.name}.get: Data received`);
                    if (options.isData) {
                        resolve(Buffer.concat(rawData));
                        return;
                    }
                    // console.debug(`${this.constructor.name}.get:`, url);

                    const parsedData = JSON.parse(Buffer.concat(rawData));
                    // log.debug(`${this.constructor.name}.get: Data parsed`);
                    resolve(parsedData);
                });
            });
            
            request.on('error', (e) => {
                reject(e);
            });

            request.on('timeout', () => {
                request.abort();
                reject(new Error(`${this.constructor.name} failed to retrieve url ${url} : socket timeout`));
            });
        });
    }
}
