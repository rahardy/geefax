// SPDX-FileCopyrightText: 2023 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { ServiceBase } from './ServiceBase.js';
import log from '../utils/logger.js';

const BASE_URL = "https://content.guardianapis.com/";

export class Guardian extends ServiceBase {
    constructor(key) {
        if (typeof key != 'string') throw new Error('E7: Guardian.constructor: key required');
        super();
        this._key = key;
        this.resetCounter();
    }

    resetCounter() {
        this._numGets = 0;
    }

    get counter() {
        return {
            gets: this._numGets,
            cacheHits: 0
        };
    }

    async picksById(id) {
        const url = `${BASE_URL}${id}?show-editors-picks=true&api-key=${this._key}&show-fields=thumbnail`;
        const res = await this.get(url);
        this._numGets++;
        const articles = res.response.editorsPicks.filter(item => item.type == 'article');
        if (articles.length == 0) {
            throw new Error(`E34: Guardian.picksById: No articles for id ${id}`);
        }
        return articles;
    }

    // used as a workaround whilst picksById is broken
    async newsById(id) {
        const url = `${BASE_URL}${id}?api-key=${this._key}&show-fields=thumbnail`;
        const res = await this.get(url);
        this._numGets++;
        const articles = res.response.results.filter(item => item.type == 'article');
        if (articles.length == 0) {
            throw new Error(`E45: Guardian.newsById: No articles for id ${id}`);
        }
        return articles;
    }

    async item(id, apiUrl) {
        const url = `${apiUrl}?api-key=${this._key}&show-fields=trailText,body,byline,thumbnail`;
        const res = await this.get(url);
        this._numGets++;

        const response = {
            webTitle:           res.response.content.webTitle,
            webPublicationDate: res.response.content.webPublicationDate,
            sectionId:          res.response.content.sectionId,
            sectionName:        res.response.content.sectionName,
            trailText:          res.response.content.fields.trailText,
            byline:             res.response.content.fields.byline,
            body:               res.response.content.fields.body,
            thumbnail:          res.response.content.fields.thumbnail
        };
        log.debug(`Guardian.item api-fetch id ${id}`);
        return response;
    }
}
