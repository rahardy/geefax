// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { ServiceBase } from './ServiceBase.js';
// import log from '../utils/logger.js';

export class ImageService extends ServiceBase {
    constructor() {
        super();
    }

    get(url) {
        // log.debug('imageservice.get', url);
        return super.get(url, { isData: true });
    }
}
