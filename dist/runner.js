// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

const commonjsbundle = require('./commonjsbundle.js');

async function run() {
    try {
        await commonjsbundle.handler();
    } catch (e) {
        console.error(e);
        process.exitCode = 1;
    }
}

run();
