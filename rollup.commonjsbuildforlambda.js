// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';

// as lambda doesn't support EcmaScript modules yet we repackage to cjs :(

export default {
  input: './src/app.js',
  output: {
      file: 'dist/commonjsbundle.js',
      format: 'cjs',
      preferConst: true,
  },
  plugins: [
    resolve({
      modulesOnly: true,
      // rollup warns about unresolved dependencies for cjs modules, but node resolves those at runtime
    }),
    json({
      preferConst: true,
      compact: true
    }),
  ],
};
