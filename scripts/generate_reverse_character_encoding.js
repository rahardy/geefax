// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import encodings from './data/characterEncodings.json';

// generate unicode to 7-bit character set mappings
function generateReversedEncodings() {
    const reversedEncodings = {};
    for (const encoding in encodings) {
        reversedEncodings[encoding] = {};

        const matches = encoding.match(/^(.+)__/);
        if (matches != null) {
            const baseEncoding = matches[1];
            for (const charTo in encodings[baseEncoding]) {
                const charFrom = encodings[baseEncoding][charTo];
                if (!(charTo in encodings[encoding]))
                    reversedEncodings[encoding][charFrom] = charTo;
            }
        }

        for (const charTo in encodings[encoding]) {
            const charFrom = encodings[encoding][charTo];
            if (charFrom != null)
                reversedEncodings[encoding][charFrom] = charTo;
        }
    }

    return reversedEncodings;
}

const reverseEncodings = generateReversedEncodings();
console.log(JSON.stringify(reverseEncodings));
