// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import { handler } from './src/app.js';

// wrapper around the lambda handler to run locally
async function run() {
    try {
        await handler();
    } catch (e) {
        console.error(e);
        process.exitCode = 1;
    }
}

run();
