// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import test from 'ava';
import * as td from 'testdouble';

import { Service } from '../../src/domain/Service.js';
import { STYLE_CONFIG } from '../../src/SectionConfigs.js';

import { ServicePipeline } from '../../src/domain/ServicePipeline.js'; // test subject

test.beforeEach(t => {
    makeMocks(t);
});

test.afterEach.always(() => {
    td.reset();
});

test('1 good story is added', async t => {
    givenServiceWithGoodStory(t);
    const pipeline = new ServicePipeline(t.context.service, STYLE_CONFIG);

    const numPagesPopulated = await pipeline.populateService();

    t.is(numPagesPopulated, 1, 'num pages populated');

    td.verify(t.context.indexAddElement(["story title  .  .  .  .  .  .  .  .101"]));
    td.verify(t.context.indexAssignPageNumbersToStories());

    const captor = td.matchers.captor();
    td.verify(t.context.pageAddElement(captor.capture(), td.matchers.anything()), { times: 2 });
    t.deepEqual(captor.values, [["story title"], ["story contents paragraph 1"]]);
});

test('a story with no paragraphs is not added', async t => {
    givenServiceWithBadStory(t);
    const pipeline = new ServicePipeline(t.context.service, STYLE_CONFIG);
    const numPagesPopulated = await pipeline.populateService();

    t.is(numPagesPopulated, 0, 'num pages populated');
    td.verify(t.context.indexAddElement(td.matchers.anything()), { times: 0 });
});


function givenServiceWithGoodStory(t) {
    const page = getMockPage(t, 'story title', ['story contents paragraph 1']);
    mock_getPage100(t, [page], ['101']);
    td.when(t.context.service.getPage('101')).thenReturn(page);
}

function givenServiceWithBadStory(t) {
    const page = getMockPage(t, 'empty story title', []);
    mock_getPage100(t, [page], []);
}

function makeMocks(t) {
    const service = td.instance(Service);
    td.replace(service, 'galleryPages', []);
    const pageAddElement = td.func('page.addElement');
    const indexAddElement = td.func('index.addElement');
    const indexAssignPageNumbersToStories = td.func('index.assignPageNumbersToStories');
    Object.assign(t.context, {
        service,
        pageAddElement,
        indexAddElement,
        indexAssignPageNumbersToStories
    });
}

function mock_getPage100(t, childPages, pageNumbers) {
    td.replace(t.context.service, 'indexPages', ['100']);
    td.when(t.context.service.getPage('100')).thenReturn({
        childPages: childPages,
        assignPageNumbersToStories: t.context.indexAssignPageNumbersToStories,
        pageNumbersInIndex: pageNumbers,
        addElement: t.context.indexAddElement
    });
}

function getMockPage(t, title, paragraphs) {
    const page = {
        id: 77,
        title: title,
        getParagraphs: () => Promise.resolve(paragraphs),
        removeFromIndex: () => void(0),
        addElement: t.context.pageAddElement
    };
    return page;
}
