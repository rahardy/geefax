// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import test from 'ava';

import { Service } from '../../src/domain/Service.js';

test('add index page to service', t => {
	const service = new Service();
    service.addIndexPage('100', {});

	const pages = service.pages;
	t.is(pages.length, 1);
	t.is(pages[0], '100');
});

test('add story pages to index', t => {
	const service = new Service();
    const index = service.addIndexPage('100', {});
	index.addStory({});
	
	t.is(index.childPages.length, 1);
});

test('page number assigned to story', t => {
	const service = new Service();
    const index = service.addIndexPage('100', {});
	index.addStory({
		contentFn: () => 'story content'
	});
	index.assignPageNumbersToStories();

	const pages = service.pages;
	t.is(pages.length, 2);
	t.deepEqual(pages, ['100', '101']);
	
	const page = service.getPage('101');
	t.is(page.contentFn(), 'story content');
});

test('page number not assigned to story when removed', t => {
	const service = new Service();
    const index = service.addIndexPage('100', {});
	index.addStory({});

	const page = index.childPages[0];
	page.removeFromIndex();
	index.assignPageNumbersToStories();

	const pages = service.pages;
	t.is(pages.length, 1);
	t.deepEqual(pages, ['100']);
});
