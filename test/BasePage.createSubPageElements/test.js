// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import test from 'ava';

import { __testExports } from '../../src/domain/Service.js';

test('paragraphs fit onto one subpage', t => {
    const elements = [
        makeElement(1), // title
        makeElement(2)  // paragraph 1
    ];
    const basePage = givenBasePageWithElements(elements);

    basePage.createSubPages(23, true);

    t.is(basePage.numSubPages, 1);
});

test('small paragraphs wraps onto extra subpage', t => {
    const elements = [
        makeElement(1),  // title
        makeElement(9),  // para 1
        makeElement(12), // para 2 - sized so it fits onto 1 subpage
    ];
    const basePage = givenBasePageWithElements(elements);

    basePage.createSubPages(23, true);

    t.is(basePage.numSubPages, 2);
    t.assert(basePage._subPages[0]._availableRows > 0); // space left over
});

test('medium sized first paragraph is split to extra subpage', t => {
    const elements = [
        makeElement(3), // title
        makeElement(21) // paragraph 1 - sized so that it would fit within 1 subpage but is split in this case
    ];
    const basePage = givenBasePageWithElements(elements);

    basePage.createSubPages(23, true);

    t.is(basePage.numSubPages, 2);
    t.is(basePage._subPages[0]._availableRows, 0);
});

test('large paragraph split to extra subpage', t => {
    const elements = [
        makeElement(1),  // title
        makeElement(9),  // para 1
        makeElement(25), // para 2 - sized so it won't fit onto 1 subpage and is split
    ];
    const basePage = givenBasePageWithElements(elements);

    basePage.createSubPages(23, true);

    t.is(basePage.numSubPages, 2);
    t.is(basePage._subPages[0]._availableRows, 0);
});


function givenBasePageWithElements(elements) {
    const basePage = new __testExports.BasePage();
    elements.forEach(e => {
        basePage.addElement(e);
    });
    return basePage;
}

function makeElement(numRowsInElement) {
    let e = [];
    for (let i = 1; i <= numRowsInElement; i++) {
        e.push(`row ${i}`);
    }
    return e;
}
