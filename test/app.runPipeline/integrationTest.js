// SPDX-FileCopyrightText: 2023 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later
/* eslint-disable ava/no-import-test-files */ // <-- to import json

import test from 'ava';
import * as td from 'testdouble';

import { SECTIONS } from '../../src/SectionConfigs.js';
import stories from './picks.json';

test.before(() => {
    process.env.GUARDIAN_API_KEY = 'fake_key';
});

test.afterEach.always(() => {
    td.reset();
});

test('pipeline runs 👌', async t => {
    await givenFakeGuardian();
    const sections = { uk: SECTIONS.uk };
    delete sections.uk.gallery;
    const { __testExports } = await import('../../src/app.js');
    const pipeline = await __testExports.runPipeline(sections);
    
    thenTestService(t, pipeline._service);
});


async function givenFakeGuardian() {
    const { Guardian } = await td.replaceEsm('../../src/services/Guardian.js');
    td.when(Guardian.prototype.picksById('uk')).thenResolve(stories);
    td.when(Guardian.prototype.newsById('uk-news')).thenResolve(stories);
    stories.forEach((story, index) => {
        td.when(Guardian.prototype.item(story.id, story.apiUrl)).thenResolve({
            body: makeStoryBody(`Story ${story.webTitle} paragraph 1`, index),
        });
    });
}

function thenTestService(t, service) {
    t.deepEqual(service.pages, ['100', '101', '102', '103'], 'service has incorrect pages');
    const snapshot = service.__test__getSnapshot();
    t.snapshot(snapshot, 'rendered TTIs incorrect');
    for (const pageNum in snapshot) {
        for (const subpage of snapshot[pageNum]) {
            t.notRegex(subpage, /{{.+}}/g, 'template slots not filled');
        }
    }
    t.regex(snapshot['103'][0], /1\/2/g, 'subpage numbering incorrect');
    t.snapshot(service.__test__getLinks(), 'page links incorrect');
}

function makeStoryBody(firstParagraph, numExtraParagraphs) {
    let body = `<p>${firstParagraph}</p>`;
    for (let i = 0; i < numExtraParagraphs; i++) {
        body += "<p>The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog.</p>\n"
    }
    return body;
}
