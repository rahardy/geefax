// SPDX-FileCopyrightText: 2021 Tech & Software Ltd.
// SPDX-License-Identifier: GPL-2.0-or-later

import test from 'ava';

import { Utils } from '../../src/utils/Utils.js';

test('paragraphs are converted', t => {
    const html = '<p>para 1</p><p>para 2</p>';
    const res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1', 'para 2']);
});

test('aside is not used', t => {
    const html = '<p>para 1</p><aside><p>aside text</p></aside>';
    const res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
});

test('figure is not used', t => {
    let html = '<p>para 1</p><figure>figure</figure>';
    let res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
    html = '<p>para 1</p><figure><iframe srcdoc="<p>para</p>"></iframe></figure>';
    res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
});

test('trailing whitespace removed', t => {
    let html = '<p>para 1   </p>';
    let res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
    html = '<p>para 1<br>  </p>';
    res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
    html = '<p>para 1<br></p>';
    res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
});

test('double br elements are split into paragraphs', t => {
    const html = '<p>para 1<br><br>next bit</p>';
    const res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1', 'next bit']);
});

test('empty paragraph removed', t => {
    let html = '<p>para 1</p><p></p>';
    let res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
    html = '<p>para 1</p><p>  <br>  </p>';
    res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1']);
});

test('br converted to newline', t => {
    let html = '<p>para 1<br>new line</p>';
    let res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1\nnew line']);
    html = '<p>para 1</p><p><strong>para 2 line 1<br>line 2</strong></p>';
    res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, ['para 1', 'para 2 line 1\nline 2']);
});

test('no paragraphs', t => {
    const html = '<figure><a href="#">link</a></figure>';
    const res = Utils.convertHTMLToParagraphs(html);
    t.deepEqual(res, []);
});
