<!-- SPDX-FileCopyrightText: 2023 Tech & Software Ltd. -->
<!-- SPDX-License-Identifier: GPL-2.0-or-later -->

Gets content from the Guardian Open Platform; formats into teletext frames in .tti files, then wraps them in JSON and saves locally or to S3.

Thumbnail images are converted to teletext mosaic pages, and they are used to create .webp images for use in web front-end meta tags.

The generated JSON is for use with https://www.npmjs.com/package/@techandsoftware/teletext-service

# Run locally

Needs node v14 as this uses ES modules.

First time:
```
npm install
```

Required env vars:

* `GUARDIAN_API_KEY` - get a key from https://open-platform.theguardian.com/access/

Optional env vars:

* `OUTPUT_BUCKET_NAME` - name of the S3 bucket to write output files to. If not set, they are written to /tmp/geefax-output
* `CACHE_BUCKET_NAME` - name of the S3 bucket used for content cacheing. If not set /tmp is used

Using the S3 buckets will need you to configure auth credentials for the AWS SDK.  See the SDK docs at https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-credentials-node.html

To invoke:
```
npm run makeservice
```
This generates .tti files in the /tmp/geefax-tti directory, and outputs to the s3 bucket if the env var above was set, or to the filesystem if not.  This runs `runner.js` which calls the lambda handler in `app.js`.

## Tests

These scripts are set up for testing:

* `npm test` - minimal output
* `npm run test:verbose` - shows every test
* `npm run test:coverage` - runs tests and generates coverage report with text output to console
* `npm run coverage-report` - generates HTML-based coverage report to `coverage/index.html` and attempts to open it in your default browser.  You need to run the `test:coverage` script first.

# Lambda

## Bundling

Lambda requires commonjs modules.  Run `npm run bundle` to generate a cjs bundle in the `dist` directory. This isn't needed if running locally.  (The 'unresolved modules' warning is expected, as they are cjs modules which aren't in the bundle.)

## Deploying

* `samtemplate.yaml` is a SAM template that will deploy to a Cloudformation stack. See the SAM CLI docs
* `bitbucket-pipelnes.yml` is a pipeline config for Bitbucket, which bundles and deploys using the SAM template. This needs the 3 env vars above to be set as repository variables, so they're picked up and fed into the SAM template as parameters.

# Notes about the character sets

Mappings are generated with the script:

`node --experimental-json-modules scripts/generate_reverse_character_encoding.js > data/unicodeToTeletextMapping.json`

The JSON maps from Unicode to teletext character sets, using the source JSON in `scripts/data` which is from the `@techandsoftware/teletext` module codebase.

When generating the composed pages in the service structure, strings are mapped to the character set defined in `app.js` (probably `g0_latin__english`).  The `mapChars` function in `Utils` also does some tidy ups for any left over non-7-bit characters.

When generating the .tti files, the file is written as latin1.  This is so that the teletext control code characters aren't mapped to utf8 and remain as single bytes.

In the `@techandsoftware/teletext` codebase, when the outputLines are parsed, this is expects single bytes, and for all character codes to be <= 0x7f, but with special provision of control codes which can be 0x80 to 0x9f and mapped back to 0x00 to 0x1f

# Guardian API issue

As of 4/1/23 - the edition front pages guardian API to retrieve the editors picks' for each edition isn't working. As a workaround, SectionConfigs.js is using other sections on the Guardian API. To undo:

1. In SectionConfigs,js, edit SECTIONS so the apiId for each section use the edition sections (e.g. uk instead of uk-news)
2. In app.js, in addSection(), call picksById instead of newsById
